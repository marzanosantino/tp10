FROM python:3.8-slim

# Instala las dependencias necesarias
RUN apt-get update && \
    apt-get install -y default-libmysqlclient-dev build-essential && \
    rm -rf /var/lib/apt/lists/*

WORKDIR /app

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

# Exponer el puerto 8080
EXPOSE 8081

CMD ["python", "app.py"]
