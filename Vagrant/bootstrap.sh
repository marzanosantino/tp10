#Docker install
sudo apt-get update
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh

#Use docker without sudo
sudo groupadd docker
sudo usermod -aG docker vagrant
newgrp docker

#Install kubectl
curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl

#Install helm
wget -O- https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash

#Install minikube
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
sudo install minikube-linux-amd64 /usr/local/bin/minikube

#install flask
pip install flask

#Clone repo
git clone https://gitlab.com/marzanosantino/tp10.git

#Start minikube cluster (Argocd)
minikube start --profile argotp10



