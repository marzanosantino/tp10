# TP10
Paso 1: Instalar Herramientas/Entorno de trabajo

En este repositorio veran un directorio llamado Vagrant, el cual nos permitira levantar una maquina virtual con las herramientas necesarias para la aplicacion (docker, minikube, kubectl, argocd, etc)

Para poder levantar esta maquina, iremos al directorio Vagrant y corremos el siguiente comando:

 vagrant up

Ya con la maquina virtual levantada, nos podremos conectar con el siguiente comando

 vagrant ssh 

Paso 2:
    -Crear la aplicación web con Flask
    -Crea un archivo llamado app.py con el siguiente contenido:

        App TP10
        """
        from flask import Flask

         app = Flask(__name__)

         @app.route('/')
         def hello_world():
         """
         Devuelve Un Hola mundo en el Puerto 8080
         """
         return '¡Hola, mundo! Esta es una aplicación Flask que escucha en el puerto 8080.'

         if __name__ == '__main__':
         app.run(debug=True, host='0.0.0.0', port=8081)


Paso 3: 
    Dockerizar la aplicación
    Crea un archivo llamado Dockerfile con el siguiente contenido:

        FROM python:3.8-slim

        # Instala las dependencias necesarias
        RUN apt-get update && \
            apt-get install -y default-libmysqlclient-dev build-essential && \
            rm -rf /var/lib/apt/lists/*

        WORKDIR /app

        COPY requirements.txt .
        RUN pip install --no-cache-dir -r requirements.txt

        COPY . .

        # Exponer el puerto 8080
        EXPOSE 8080

        CMD ["python", "app.py"]




Paso 4: 
    Construir y ejecutar la imagen Docker
    Abre una terminal en el directorio de tu aplicación y ejecuta los siguientes comandos:

        docker build -t apptp10 .
        docker run -p 8080:8080 apptp10

Paso 5: 
    Verifica la aplicación en tu navegador
    Abre tu navegador y visita https://127.17.0.2:8080/ para asegurarte de que la aplicación funcione correctamente.
 

Paso 6:
    Automatización de Dockerfile
    Para ello se creo el siguiente archivo .gitlab-ci.yml en GitlabCI/CD

        stages:
        - build
        - test

        variables:
        DOCKER_IMAGE_NAME: santinotona/tp10v2:latest

        docker-build:
        image: docker:latest
        stage: build
        services:
            - docker:dind
        before_script:
            - docker login -u "$CI_REGISTRY_USER" -p dckr_pat_cyDPRYar7pkSRXCjxswvI8LdYRg
        script:
            - echo "Etapa build "
            - docker build --pull -t "$DOCKER_IMAGE_NAME" .
            - docker push "$DOCKER_IMAGE_NAME"
        test-job:
        stage: test
        image: python:3.12.0rc3-alpine3.18
        script:
            - pip install flask
            - flask --version
            - echo "Ejecutando el linter (flake8)..."
            - pip3 install flake8
            - flake8 . --exclude=migrations --count --select=E9,F63,F7,F82 --show-source --statistics
            - pip install pylint pylint-junit
            - pylint --output-format=pylint_junit.JUnitReporter app.py | tee report.xml
            #- ls -la
        artifacts:
            paths:
            - report.xml
        when: always



Paso 7:
    Ya habiendo contenerizado las distintas partes de la aplicación y automatizado la construcción de la imagen, toca el turno de convertir estos contenedores en recursos de Kubernetes.
    Mediante la creación de los siguientes manifiestos .yaml

    En primer lugar crear el cluster de minikube:

    minikube start --profile argotp10


    Deployment.yaml

        apiVersion: apps/v1
        kind: Deployment
        metadata:
        name: mi-app
        spec:
        replicas: 1
        selector:
            matchLabels:
            app: mi-app
        template:
            metadata:
            labels:
                app: mi-app
            spec:
            containers:
            - name: mi-app
                image: santinotona/tp10v2:latest
                ports:
                - containerPort: 8080

    Service.yaml

        apiVersion: v1
        kind: Service
        metadata:
        name: mi-app-service
        spec:
        selector:
            app: mi-app
        ports:
            - protocol: TCP
            port: 80
            targetPort: 8080


Paso 8:
     Después de crear estos archivos YAML, puedes aplicarlos a tu clúster de Kubernetes usando el siguiente comando:

        kubectl apply -f deployment.yaml -n app
        kubectl apply -f service.yaml -n app

Paso 9:
    Finalmente, puedes usar el port-forward de kubectl junto con el puente de Vagrant SSH para acceder a tu aplicación. Por ejemplo:

        kubectl port-forward service/myapp-service 8080:80

Paso 10: ArgoCD

Por ultimo, tendremos la parte de ArgoCD. Podremos crear aplicaciones ya sea utilizando la CLI o utilizando la interfaz grafica de Argo. En este caso utilizaremos el archivo deployment.yaml en el directorio de tp10. Por ejemplo si quisieramos hacerlo con el ArgoCD CLI:




