#from flask import Flask

#app = Flask(__name__)

#@app.route('/')
#def hello_world():
   #return '¡Hola, mundo! Esta es una aplicación Flask que escucha en el puerto 8080.'

#if __name__ == '__main__':
   #app.run(debug=True, host='0.0.0.0', port=8080)
"""
App TP10
"""
from flask import Flask

app = Flask(__name__)

@app.route('/')
def hello_world():
    """
    Devuelve Un Hola mundo en el Puerto 8080
    """
    return '¡Hola, mundo! Esta es una aplicación Flask que escucha en el puerto 8080.'

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8081)
